# Bash Setup

## Overview

Bring colour into your life! Displays the current git status, branch and current working directory in colour at the bash prompt.

![Screenshot](https://bitbucket.org/josephearl/bash-setup/raw/master/screenshot.png)

## Getting Started

Simply copy all of the files (including the hidden ones) into your Home folder.

	$ cp -r bash-setup/ ~
	
Then add the following to the end of your `.bash_profile`

    # Source bashrc if it exists
    if [ -f ~/.bashrc ]; then
        source ~/.bashrc
    fi
	
Quit Terminal and re-open.